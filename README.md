# Routes

`POST /users`
Params `username` and `password`

`PUT /users/{username}`
Params `password`

`DELETE /users/{username}`

## Running

To start a web server for the application, run:

    lein ring server
